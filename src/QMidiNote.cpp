/*
 * Copyright 2018-2019 Quentin Vignaud
 * All rights reserved. Distributed under the terms of the MIT license.
 */

#include "QMidiNote.h"
#include <cmath>

QMidiNoteException::QMidiNoteException(QMidiNoteException::Reason reason, int note) : reason(reason), note(note)
{

}

QMidiNoteException *QMidiNoteException::clone() const { return new QMidiNoteException(*this); }

void QMidiNoteException::raise() const { throw *this; }

QMidiNote::QMidiNote(int note) : _note(0)
{
    setNote(note);
}

int QMidiNote::note() const
{
    return _note;
}

void QMidiNote::setNote(int note)
{
    if (note >= 0 && note <= 127) _note = note;
    else throw QMidiNoteException(QMidiNoteException::OutOfBounds, note);
}

int QMidiNote::number() const
{
    return _note % 12;
}

int QMidiNote::octave() const
{
    return (_note / 12) - 1;
}

double QMidiNote::frequency() const
{
    const double fundamental = 440;
    const int numberFundamental = 69;
    const int semitoneCount = 12;

    return fundamental * pow(2, (_note - numberFundamental) / semitoneCount);
}

QString QMidiNote::toString() const
{
    QLocale::Language language = QLocale::system().language();
    QString noteString;

    if (language == QLocale::English) {
        switch (number()) {
            case 0:
                noteString = "C";
            break;
            case 1:
                noteString = "C#";
            break;
            case 2:
                noteString = "D";
            break;
            case 3:
                noteString = "D#";
            break;
            case 4:
                noteString = "E";
            break;
            case 5:
                noteString = "F";
            break;
            case 6:
                noteString = "F#";
            break;
            case 7:
                noteString = "G";
            break;
            case 8:
                noteString = "G#";
            break;
            case 9:
                noteString = "A";
            break;
            case 10:
                noteString = "A#";
            break;
            case 11:
                noteString = "B";
            break;
        }
    }
    else {
        switch (number()) {
            case 0:
                noteString = "Do";
            break;
            case 1:
                noteString = "Do#";
            break;
            case 2:
                noteString = "Re";
            break;
            case 3:
                noteString = "Re#";
            break;
            case 4:
                noteString = "Mi";
            break;
            case 5:
                noteString = "Fa";
            break;
            case 6:
                noteString = "Fa#";
            break;
            case 7:
                noteString = "Sol";
            break;
            case 8:
                noteString = "Sol#";
            break;
            case 9:
                noteString = "La";
            break;
            case 10:
                noteString = "La#";
            break;
            case 11:
                noteString = "Si";
            break;
        }
    }

    noteString += QString::number(octave());

    return noteString;
}

QString QMidiNote::name() const
{
    QLocale::Language language = QLocale::system().language();
    if (language == QLocale::English) {
        switch (number()) {
            case 0:
                return "C";

            case 1:
                return "C♯";

            case 2:
                return "D";

            case 3:
                return "D♯";

            case 4:
                return "E";

            case 5:
                return "F";

            case 6:
                return "F♯";

            case 7:
                return "G";

            case 8:
                return "G♯";

            case 9:
                return "A";

            case 10:
                return "A♯";

            case 11:
                return "B";
        }
    }
    else {
        switch (number()) {
            case 0:
                return "Do";

            case 1:
                return "Do♯";

            case 2:
                return "Ré";

            case 3:
                return "Ré♯";

            case 4:
                return "Mi";

            case 5:
                return "Fa";

            case 6:
                return "Fa♯";

            case 7:
                return "Sol";

            case 8:
                return "Sol♯";

            case 9:
                return "La";

            case 10:
                return "La♯";

            case 11:
                return "Si";
        }
    }

    return "";
}

QMidiNote QMidiNote::operator=(const QMidiNote &n) {
    setNote(n._note);
    return *this;
}

QMidiNote QMidiNote::operator+(const QMidiNote &n) {
    return QMidiNote(_note + n._note);
}

QMidiNote QMidiNote::operator+=(const QMidiNote &n) {
    setNote(_note + n._note);
    return *this;
}

QMidiNote QMidiNote::operator++() {
    setNote(_note+1);
    return *this;
}

QMidiNote QMidiNote::operator-(const QMidiNote &n) {
    return QMidiNote(_note - n._note);
}

QMidiNote QMidiNote::operator-=(const QMidiNote &n) {
    setNote(_note - n._note);
    return *this;
}

QMidiNote QMidiNote::operator--() {
    setNote(_note-1);
    return *this;
}

bool QMidiNote::operator<(const QMidiNote &n) {
    return _note < n._note;
}

bool QMidiNote::operator<=(const QMidiNote &n) {
    return _note <= n._note;
}

bool QMidiNote::operator>(const QMidiNote &n) {
    return _note > n._note;
}

bool QMidiNote::operator>=(const QMidiNote &n) {
    return _note >= n._note;
}

bool QMidiNote::operator==(const QMidiNote &n) {
    return _note == n._note;
}

bool QMidiNote::operator!=(const QMidiNote &n) {
    return _note != n._note;
}

QString QMidiNote::toString(int note)
{
    return QMidiNote(note).toString();
}

QMidiNote QMidiNote::fromString(QString name)
{
    if (QString::number(0) == name
        || name.compare("Do-1", Qt::CaseInsensitive) == 0
        || name.compare("Sid-1", Qt::CaseInsensitive) == 0
        || name.compare("SiM-1", Qt::CaseInsensitive) == 0
        || name.compare("C-1", Qt::CaseInsensitive) == 0
        || name.compare("Bd-1", Qt::CaseInsensitive) == 0
        || name.compare("BM-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(0);
    }

    else if (QString::number(1) == name
        || name.compare("Dod-1", Qt::CaseInsensitive) == 0
        || name.compare("DoM-1", Qt::CaseInsensitive) == 0
        || name.compare("Reb-1", Qt::CaseInsensitive) == 0
        || name.compare("Rem-1", Qt::CaseInsensitive) == 0
        || name.compare("Do#-1", Qt::CaseInsensitive) == 0
        || name.compare("Do♯-1", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭-1", Qt::CaseInsensitive) == 0
        || name.compare("Re♭-1", Qt::CaseInsensitive) == 0
        || name.compare("Réb-1", Qt::CaseInsensitive) == 0
        || name.compare("Rém-1", Qt::CaseInsensitive) == 0
        || name.compare("Cd-1", Qt::CaseInsensitive) == 0
        || name.compare("CM-1", Qt::CaseInsensitive) == 0
        || name.compare("Db-1", Qt::CaseInsensitive) == 0
        || name.compare("Dm-1", Qt::CaseInsensitive) == 0
        || name.compare("C#-1", Qt::CaseInsensitive) == 0
        || name.compare("C♯-1", Qt::CaseInsensitive) == 0
        || name.compare("D♭-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(1);
    }

    else if (QString::number(2) == name
        || name.compare("Re-1", Qt::CaseInsensitive) == 0
        || name.compare("Ré-1", Qt::CaseInsensitive) == 0
        || name.compare("D-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(2);
    }

    else if (QString::number(3) == name
        || name.compare("Red-1", Qt::CaseInsensitive) == 0
        || name.compare("ReM-1", Qt::CaseInsensitive) == 0
        || name.compare("Mib-1", Qt::CaseInsensitive) == 0
        || name.compare("Mim-1", Qt::CaseInsensitive) == 0
        || name.compare("Re#-1", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯-1", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭-1", Qt::CaseInsensitive) == 0
        || name.compare("Re♯-1", Qt::CaseInsensitive) == 0
        || name.compare("Ré#-1", Qt::CaseInsensitive) == 0
        || name.compare("Réd-1", Qt::CaseInsensitive) == 0
        || name.compare("RéM-1", Qt::CaseInsensitive) == 0
        || name.compare("Dd-1", Qt::CaseInsensitive) == 0
        || name.compare("DM-1", Qt::CaseInsensitive) == 0
        || name.compare("Eb-1", Qt::CaseInsensitive) == 0
        || name.compare("Em-1", Qt::CaseInsensitive) == 0
        || name.compare("D#-1", Qt::CaseInsensitive) == 0
        || name.compare("D♯-1", Qt::CaseInsensitive) == 0
        || name.compare("E♭-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(3);
    }

    else if (QString::number(4) == name
        || name.compare("Mi-1", Qt::CaseInsensitive) == 0
        || name.compare("Fab-1", Qt::CaseInsensitive) == 0
        || name.compare("Fam-1", Qt::CaseInsensitive) == 0
        || name.compare("E-1", Qt::CaseInsensitive) == 0
        || name.compare("Fb-1", Qt::CaseInsensitive) == 0
        || name.compare("Fm-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(4);
    }

    else if (QString::number(5) == name
        || name.compare("Fa-1", Qt::CaseInsensitive) == 0
        || name.compare("Mid-1", Qt::CaseInsensitive) == 0
        || name.compare("MiM-1", Qt::CaseInsensitive) == 0
        || name.compare("F-1", Qt::CaseInsensitive) == 0
        || name.compare("Ed-1", Qt::CaseInsensitive) == 0
        || name.compare("EM-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(5);
    }

    else if (QString::number(6) == name
        || name.compare("Fad-1", Qt::CaseInsensitive) == 0
        || name.compare("FaM-1", Qt::CaseInsensitive) == 0
        || name.compare("Solb-1", Qt::CaseInsensitive) == 0
        || name.compare("Solm-1", Qt::CaseInsensitive) == 0
        || name.compare("Fa#-1", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯-1", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭-1", Qt::CaseInsensitive) == 0
        || name.compare("Fd-1", Qt::CaseInsensitive) == 0
        || name.compare("FM-1", Qt::CaseInsensitive) == 0
        || name.compare("Gb-1", Qt::CaseInsensitive) == 0
        || name.compare("Gm-1", Qt::CaseInsensitive) == 0
        || name.compare("F#-1", Qt::CaseInsensitive) == 0
        || name.compare("F♯-1", Qt::CaseInsensitive) == 0
        || name.compare("G♭-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(6);
    }

    else if (QString::number(7) == name
        || name.compare("Sol-1", Qt::CaseInsensitive) == 0
        || name.compare("G-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(7);
    }

    else if (QString::number(8) == name
        || name.compare("Sold-1", Qt::CaseInsensitive) == 0
        || name.compare("SolM-1", Qt::CaseInsensitive) == 0
        || name.compare("Lab-1", Qt::CaseInsensitive) == 0
        || name.compare("Lam-1", Qt::CaseInsensitive) == 0
        || name.compare("Sol#-1", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯-1", Qt::CaseInsensitive) == 0
        || name.compare("La♭-1", Qt::CaseInsensitive) == 0
        || name.compare("Gd-1", Qt::CaseInsensitive) == 0
        || name.compare("GM-1", Qt::CaseInsensitive) == 0
        || name.compare("Ab-1", Qt::CaseInsensitive) == 0
        || name.compare("Am-1", Qt::CaseInsensitive) == 0
        || name.compare("G#-1", Qt::CaseInsensitive) == 0
        || name.compare("G♯-1", Qt::CaseInsensitive) == 0
        || name.compare("A♭-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(8);
    }

    else if (QString::number(9) == name
        || name.compare("La-1", Qt::CaseInsensitive) == 0
        || name.compare("A-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(9);
    }

    else if (QString::number(10) == name
        || name.compare("Lad-1", Qt::CaseInsensitive) == 0
        || name.compare("LaM-1", Qt::CaseInsensitive) == 0
        || name.compare("Sib-1", Qt::CaseInsensitive) == 0
        || name.compare("Sim-1", Qt::CaseInsensitive) == 0
        || name.compare("La#-1", Qt::CaseInsensitive) == 0
        || name.compare("La♯-1", Qt::CaseInsensitive) == 0
        || name.compare("Si♭-1", Qt::CaseInsensitive) == 0
        || name.compare("Ad-1", Qt::CaseInsensitive) == 0
        || name.compare("AM-1", Qt::CaseInsensitive) == 0
        || name.compare("Bb-1", Qt::CaseInsensitive) == 0
        || name.compare("Bm-1", Qt::CaseInsensitive) == 0
        || name.compare("A#-1", Qt::CaseInsensitive) == 0
        || name.compare("A♯-1", Qt::CaseInsensitive) == 0
        || name.compare("B♭-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(10);
    }

    else if (QString::number(11) == name
        || name.compare("Si-1", Qt::CaseInsensitive) == 0
        || name.compare("Dob-1", Qt::CaseInsensitive) == 0
        || name.compare("Dom-1", Qt::CaseInsensitive) == 0
        || name.compare("B-1", Qt::CaseInsensitive) == 0
        || name.compare("Cb-1", Qt::CaseInsensitive) == 0
        || name.compare("Cm-1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(11);
    }

    else if (QString::number(12) == name
        || name.compare("Do0", Qt::CaseInsensitive) == 0
        || name.compare("Sid0", Qt::CaseInsensitive) == 0
        || name.compare("SiM0", Qt::CaseInsensitive) == 0
        || name.compare("C0", Qt::CaseInsensitive) == 0
        || name.compare("Bd0", Qt::CaseInsensitive) == 0
        || name.compare("BM0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(12);
    }

    else if (QString::number(13) == name
        || name.compare("Dod0", Qt::CaseInsensitive) == 0
        || name.compare("DoM0", Qt::CaseInsensitive) == 0
        || name.compare("Reb0", Qt::CaseInsensitive) == 0
        || name.compare("Rem0", Qt::CaseInsensitive) == 0
        || name.compare("Do#0", Qt::CaseInsensitive) == 0
        || name.compare("Do♯0", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭0", Qt::CaseInsensitive) == 0
        || name.compare("Re♭0", Qt::CaseInsensitive) == 0
        || name.compare("Réb0", Qt::CaseInsensitive) == 0
        || name.compare("Rém0", Qt::CaseInsensitive) == 0
        || name.compare("Cd0", Qt::CaseInsensitive) == 0
        || name.compare("CM0", Qt::CaseInsensitive) == 0
        || name.compare("Db0", Qt::CaseInsensitive) == 0
        || name.compare("Dm0", Qt::CaseInsensitive) == 0
        || name.compare("C#0", Qt::CaseInsensitive) == 0
        || name.compare("C♯0", Qt::CaseInsensitive) == 0
        || name.compare("D♭0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(13);
    }

    else if (QString::number(14) == name
        || name.compare("Re0", Qt::CaseInsensitive) == 0
        || name.compare("Ré0", Qt::CaseInsensitive) == 0
        || name.compare("D0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(14);
    }

    else if (QString::number(15) == name
        || name.compare("Red0", Qt::CaseInsensitive) == 0
        || name.compare("ReM0", Qt::CaseInsensitive) == 0
        || name.compare("Mib0", Qt::CaseInsensitive) == 0
        || name.compare("Mim0", Qt::CaseInsensitive) == 0
        || name.compare("Re#0", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯0", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭0", Qt::CaseInsensitive) == 0
        || name.compare("Re♯0", Qt::CaseInsensitive) == 0
        || name.compare("Ré#0", Qt::CaseInsensitive) == 0
        || name.compare("Réd0", Qt::CaseInsensitive) == 0
        || name.compare("RéM0", Qt::CaseInsensitive) == 0
        || name.compare("Dd0", Qt::CaseInsensitive) == 0
        || name.compare("DM0", Qt::CaseInsensitive) == 0
        || name.compare("Eb0", Qt::CaseInsensitive) == 0
        || name.compare("Em0", Qt::CaseInsensitive) == 0
        || name.compare("D#0", Qt::CaseInsensitive) == 0
        || name.compare("D♯0", Qt::CaseInsensitive) == 0
        || name.compare("E♭0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(15);
    }

    else if (QString::number(16) == name
        || name.compare("Mi0", Qt::CaseInsensitive) == 0
        || name.compare("Fab0", Qt::CaseInsensitive) == 0
        || name.compare("Fam0", Qt::CaseInsensitive) == 0
        || name.compare("E0", Qt::CaseInsensitive) == 0
        || name.compare("Fb0", Qt::CaseInsensitive) == 0
        || name.compare("Fm0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(16);
    }

    else if (QString::number(17) == name
        || name.compare("Fa0", Qt::CaseInsensitive) == 0
        || name.compare("Mid0", Qt::CaseInsensitive) == 0
        || name.compare("MiM0", Qt::CaseInsensitive) == 0
        || name.compare("F0", Qt::CaseInsensitive) == 0
        || name.compare("Ed0", Qt::CaseInsensitive) == 0
        || name.compare("EM0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(17);
    }

    else if (QString::number(18) == name
        || name.compare("Fad0", Qt::CaseInsensitive) == 0
        || name.compare("FaM0", Qt::CaseInsensitive) == 0
        || name.compare("Solb0", Qt::CaseInsensitive) == 0
        || name.compare("Solm0", Qt::CaseInsensitive) == 0
        || name.compare("Fa#0", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯0", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭0", Qt::CaseInsensitive) == 0
        || name.compare("Fd0", Qt::CaseInsensitive) == 0
        || name.compare("FM0", Qt::CaseInsensitive) == 0
        || name.compare("Gb0", Qt::CaseInsensitive) == 0
        || name.compare("Gm0", Qt::CaseInsensitive) == 0
        || name.compare("F#0", Qt::CaseInsensitive) == 0
        || name.compare("F♯0", Qt::CaseInsensitive) == 0
        || name.compare("G♭0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(18);
    }

    else if (QString::number(19) == name
        || name.compare("Sol0", Qt::CaseInsensitive) == 0
        || name.compare("G0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(19);
    }

    else if (QString::number(20) == name
        || name.compare("Sold0", Qt::CaseInsensitive) == 0
        || name.compare("SolM0", Qt::CaseInsensitive) == 0
        || name.compare("Lab0", Qt::CaseInsensitive) == 0
        || name.compare("Lam0", Qt::CaseInsensitive) == 0
        || name.compare("Sol#0", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯0", Qt::CaseInsensitive) == 0
        || name.compare("La♭0", Qt::CaseInsensitive) == 0
        || name.compare("Gd0", Qt::CaseInsensitive) == 0
        || name.compare("GM0", Qt::CaseInsensitive) == 0
        || name.compare("Ab0", Qt::CaseInsensitive) == 0
        || name.compare("Am0", Qt::CaseInsensitive) == 0
        || name.compare("G#0", Qt::CaseInsensitive) == 0
        || name.compare("G♯0", Qt::CaseInsensitive) == 0
        || name.compare("A♭0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(20);
    }

    else if (QString::number(21) == name
        || name.compare("La0", Qt::CaseInsensitive) == 0
        || name.compare("A0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(21);
    }

    else if (QString::number(22) == name
        || name.compare("Lad0", Qt::CaseInsensitive) == 0
        || name.compare("LaM0", Qt::CaseInsensitive) == 0
        || name.compare("Sib0", Qt::CaseInsensitive) == 0
        || name.compare("Sim0", Qt::CaseInsensitive) == 0
        || name.compare("La#0", Qt::CaseInsensitive) == 0
        || name.compare("La♯0", Qt::CaseInsensitive) == 0
        || name.compare("Si♭0", Qt::CaseInsensitive) == 0
        || name.compare("Ad0", Qt::CaseInsensitive) == 0
        || name.compare("AM0", Qt::CaseInsensitive) == 0
        || name.compare("Bb0", Qt::CaseInsensitive) == 0
        || name.compare("Bm0", Qt::CaseInsensitive) == 0
        || name.compare("A#0", Qt::CaseInsensitive) == 0
        || name.compare("A♯0", Qt::CaseInsensitive) == 0
        || name.compare("B♭0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(22);
    }

    else if (QString::number(23) == name
        || name.compare("Si0", Qt::CaseInsensitive) == 0
        || name.compare("Dob0", Qt::CaseInsensitive) == 0
        || name.compare("Dom0", Qt::CaseInsensitive) == 0
        || name.compare("B0", Qt::CaseInsensitive) == 0
        || name.compare("Cb0", Qt::CaseInsensitive) == 0
        || name.compare("Cm0", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(23);
    }

    else if (QString::number(24) == name
        || name.compare("Do1", Qt::CaseInsensitive) == 0
        || name.compare("Sid1", Qt::CaseInsensitive) == 0
        || name.compare("SiM1", Qt::CaseInsensitive) == 0
        || name.compare("C1", Qt::CaseInsensitive) == 0
        || name.compare("Bd1", Qt::CaseInsensitive) == 0
        || name.compare("BM1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(24);
    }

    else if (QString::number(25) == name
        || name.compare("Dod1", Qt::CaseInsensitive) == 0
        || name.compare("DoM1", Qt::CaseInsensitive) == 0
        || name.compare("Reb1", Qt::CaseInsensitive) == 0
        || name.compare("Rem1", Qt::CaseInsensitive) == 0
        || name.compare("Do#1", Qt::CaseInsensitive) == 0
        || name.compare("Do♯1", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭1", Qt::CaseInsensitive) == 0
        || name.compare("Re♭1", Qt::CaseInsensitive) == 0
        || name.compare("Réb1", Qt::CaseInsensitive) == 0
        || name.compare("Rém1", Qt::CaseInsensitive) == 0
        || name.compare("Cd1", Qt::CaseInsensitive) == 0
        || name.compare("CM1", Qt::CaseInsensitive) == 0
        || name.compare("Db1", Qt::CaseInsensitive) == 0
        || name.compare("Dm1", Qt::CaseInsensitive) == 0
        || name.compare("C#1", Qt::CaseInsensitive) == 0
        || name.compare("C♯1", Qt::CaseInsensitive) == 0
        || name.compare("D♭1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(25);
    }

    else if (QString::number(26) == name
        || name.compare("Re1", Qt::CaseInsensitive) == 0
        || name.compare("Ré1", Qt::CaseInsensitive) == 0
        || name.compare("D1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(26);
    }

    else if (QString::number(27) == name
        || name.compare("Red1", Qt::CaseInsensitive) == 0
        || name.compare("ReM1", Qt::CaseInsensitive) == 0
        || name.compare("Mib1", Qt::CaseInsensitive) == 0
        || name.compare("Mim1", Qt::CaseInsensitive) == 0
        || name.compare("Re#1", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯1", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭1", Qt::CaseInsensitive) == 0
        || name.compare("Re♯1", Qt::CaseInsensitive) == 0
        || name.compare("Ré#1", Qt::CaseInsensitive) == 0
        || name.compare("Réd1", Qt::CaseInsensitive) == 0
        || name.compare("RéM1", Qt::CaseInsensitive) == 0
        || name.compare("Dd1", Qt::CaseInsensitive) == 0
        || name.compare("DM1", Qt::CaseInsensitive) == 0
        || name.compare("Eb1", Qt::CaseInsensitive) == 0
        || name.compare("Em1", Qt::CaseInsensitive) == 0
        || name.compare("D#1", Qt::CaseInsensitive) == 0
        || name.compare("D♯1", Qt::CaseInsensitive) == 0
        || name.compare("E♭1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(27);
    }

    else if (QString::number(28) == name
        || name.compare("Mi1", Qt::CaseInsensitive) == 0
        || name.compare("Fab1", Qt::CaseInsensitive) == 0
        || name.compare("Fam1", Qt::CaseInsensitive) == 0
        || name.compare("E1", Qt::CaseInsensitive) == 0
        || name.compare("Fb1", Qt::CaseInsensitive) == 0
        || name.compare("Fm1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(28);
    }

    else if (QString::number(29) == name
        || name.compare("Fa1", Qt::CaseInsensitive) == 0
        || name.compare("Mid1", Qt::CaseInsensitive) == 0
        || name.compare("MiM1", Qt::CaseInsensitive) == 0
        || name.compare("F1", Qt::CaseInsensitive) == 0
        || name.compare("Ed1", Qt::CaseInsensitive) == 0
        || name.compare("EM1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(29);
    }

    else if (QString::number(30) == name
        || name.compare("Fad1", Qt::CaseInsensitive) == 0
        || name.compare("FaM1", Qt::CaseInsensitive) == 0
        || name.compare("Solb1", Qt::CaseInsensitive) == 0
        || name.compare("Solm1", Qt::CaseInsensitive) == 0
        || name.compare("Fa#1", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯1", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭1", Qt::CaseInsensitive) == 0
        || name.compare("Fd1", Qt::CaseInsensitive) == 0
        || name.compare("FM1", Qt::CaseInsensitive) == 0
        || name.compare("Gb1", Qt::CaseInsensitive) == 0
        || name.compare("Gm1", Qt::CaseInsensitive) == 0
        || name.compare("F#1", Qt::CaseInsensitive) == 0
        || name.compare("F♯1", Qt::CaseInsensitive) == 0
        || name.compare("G♭1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(30);
    }

    else if (QString::number(31) == name
        || name.compare("Sol1", Qt::CaseInsensitive) == 0
        || name.compare("G1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(31);
    }

    else if (QString::number(32) == name
        || name.compare("Sold1", Qt::CaseInsensitive) == 0
        || name.compare("SolM1", Qt::CaseInsensitive) == 0
        || name.compare("Lab1", Qt::CaseInsensitive) == 0
        || name.compare("Lam1", Qt::CaseInsensitive) == 0
        || name.compare("Sol#1", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯1", Qt::CaseInsensitive) == 0
        || name.compare("La♭1", Qt::CaseInsensitive) == 0
        || name.compare("Gd1", Qt::CaseInsensitive) == 0
        || name.compare("GM1", Qt::CaseInsensitive) == 0
        || name.compare("Ab1", Qt::CaseInsensitive) == 0
        || name.compare("Am1", Qt::CaseInsensitive) == 0
        || name.compare("G#1", Qt::CaseInsensitive) == 0
        || name.compare("G♯1", Qt::CaseInsensitive) == 0
        || name.compare("A♭1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(32);
    }

    else if (QString::number(33) == name
        || name.compare("La1", Qt::CaseInsensitive) == 0
        || name.compare("A1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(33);
    }

    else if (QString::number(34) == name
        || name.compare("Lad1", Qt::CaseInsensitive) == 0
        || name.compare("LaM1", Qt::CaseInsensitive) == 0
        || name.compare("Sib1", Qt::CaseInsensitive) == 0
        || name.compare("Sim1", Qt::CaseInsensitive) == 0
        || name.compare("La#1", Qt::CaseInsensitive) == 0
        || name.compare("La♯1", Qt::CaseInsensitive) == 0
        || name.compare("Si♭1", Qt::CaseInsensitive) == 0
        || name.compare("Ad1", Qt::CaseInsensitive) == 0
        || name.compare("AM1", Qt::CaseInsensitive) == 0
        || name.compare("Bb1", Qt::CaseInsensitive) == 0
        || name.compare("Bm1", Qt::CaseInsensitive) == 0
        || name.compare("A#1", Qt::CaseInsensitive) == 0
        || name.compare("A♯1", Qt::CaseInsensitive) == 0
        || name.compare("B♭1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(34);
    }

    else if (QString::number(35) == name
        || name.compare("Si1", Qt::CaseInsensitive) == 0
        || name.compare("Dob1", Qt::CaseInsensitive) == 0
        || name.compare("Dom1", Qt::CaseInsensitive) == 0
        || name.compare("B1", Qt::CaseInsensitive) == 0
        || name.compare("Cb1", Qt::CaseInsensitive) == 0
        || name.compare("Cm1", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(35);
    }

    else if (QString::number(36) == name
        || name.compare("Do2", Qt::CaseInsensitive) == 0
        || name.compare("Sid2", Qt::CaseInsensitive) == 0
        || name.compare("SiM2", Qt::CaseInsensitive) == 0
        || name.compare("C2", Qt::CaseInsensitive) == 0
        || name.compare("Bd2", Qt::CaseInsensitive) == 0
        || name.compare("BM2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(36);
    }

    else if (QString::number(37) == name
        || name.compare("Dod2", Qt::CaseInsensitive) == 0
        || name.compare("DoM2", Qt::CaseInsensitive) == 0
        || name.compare("Reb2", Qt::CaseInsensitive) == 0
        || name.compare("Rem2", Qt::CaseInsensitive) == 0
        || name.compare("Do#2", Qt::CaseInsensitive) == 0
        || name.compare("Do♯2", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭2", Qt::CaseInsensitive) == 0
        || name.compare("Re♭2", Qt::CaseInsensitive) == 0
        || name.compare("Réb2", Qt::CaseInsensitive) == 0
        || name.compare("Rém2", Qt::CaseInsensitive) == 0
        || name.compare("Cd2", Qt::CaseInsensitive) == 0
        || name.compare("CM2", Qt::CaseInsensitive) == 0
        || name.compare("Db2", Qt::CaseInsensitive) == 0
        || name.compare("Dm2", Qt::CaseInsensitive) == 0
        || name.compare("C#2", Qt::CaseInsensitive) == 0
        || name.compare("C♯2", Qt::CaseInsensitive) == 0
        || name.compare("D♭2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(37);
    }

    else if (QString::number(38) == name
        || name.compare("Re2", Qt::CaseInsensitive) == 0
        || name.compare("Ré2", Qt::CaseInsensitive) == 0
        || name.compare("D2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(38);
    }

    else if (QString::number(39) == name
        || name.compare("Red2", Qt::CaseInsensitive) == 0
        || name.compare("ReM2", Qt::CaseInsensitive) == 0
        || name.compare("Mib2", Qt::CaseInsensitive) == 0
        || name.compare("Mim2", Qt::CaseInsensitive) == 0
        || name.compare("Re#2", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯2", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭2", Qt::CaseInsensitive) == 0
        || name.compare("Re♯2", Qt::CaseInsensitive) == 0
        || name.compare("Ré#2", Qt::CaseInsensitive) == 0
        || name.compare("Réd2", Qt::CaseInsensitive) == 0
        || name.compare("RéM2", Qt::CaseInsensitive) == 0
        || name.compare("Dd2", Qt::CaseInsensitive) == 0
        || name.compare("DM2", Qt::CaseInsensitive) == 0
        || name.compare("Eb2", Qt::CaseInsensitive) == 0
        || name.compare("Em2", Qt::CaseInsensitive) == 0
        || name.compare("D#2", Qt::CaseInsensitive) == 0
        || name.compare("D♯2", Qt::CaseInsensitive) == 0
        || name.compare("E♭2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(39);
    }

    else if (QString::number(40) == name
        || name.compare("Mi2", Qt::CaseInsensitive) == 0
        || name.compare("Fab2", Qt::CaseInsensitive) == 0
        || name.compare("Fam2", Qt::CaseInsensitive) == 0
        || name.compare("E2", Qt::CaseInsensitive) == 0
        || name.compare("Fb2", Qt::CaseInsensitive) == 0
        || name.compare("Fm2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(40);
    }

    else if (QString::number(41) == name
        || name.compare("Fa2", Qt::CaseInsensitive) == 0
        || name.compare("Mid2", Qt::CaseInsensitive) == 0
        || name.compare("MiM2", Qt::CaseInsensitive) == 0
        || name.compare("F2", Qt::CaseInsensitive) == 0
        || name.compare("Ed2", Qt::CaseInsensitive) == 0
        || name.compare("EM2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(41);
    }

    else if (QString::number(42) == name
        || name.compare("Fad2", Qt::CaseInsensitive) == 0
        || name.compare("FaM2", Qt::CaseInsensitive) == 0
        || name.compare("Solb2", Qt::CaseInsensitive) == 0
        || name.compare("Solm2", Qt::CaseInsensitive) == 0
        || name.compare("Fa#2", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯2", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭2", Qt::CaseInsensitive) == 0
        || name.compare("Fd2", Qt::CaseInsensitive) == 0
        || name.compare("FM2", Qt::CaseInsensitive) == 0
        || name.compare("Gb2", Qt::CaseInsensitive) == 0
        || name.compare("Gm2", Qt::CaseInsensitive) == 0
        || name.compare("F#2", Qt::CaseInsensitive) == 0
        || name.compare("F♯2", Qt::CaseInsensitive) == 0
        || name.compare("G♭2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(42);
    }

    else if (QString::number(43) == name
        || name.compare("Sol2", Qt::CaseInsensitive) == 0
        || name.compare("G2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(43);
    }

    else if (QString::number(44) == name
        || name.compare("Sold2", Qt::CaseInsensitive) == 0
        || name.compare("SolM2", Qt::CaseInsensitive) == 0
        || name.compare("Lab2", Qt::CaseInsensitive) == 0
        || name.compare("Lam2", Qt::CaseInsensitive) == 0
        || name.compare("Sol#2", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯2", Qt::CaseInsensitive) == 0
        || name.compare("La♭2", Qt::CaseInsensitive) == 0
        || name.compare("Gd2", Qt::CaseInsensitive) == 0
        || name.compare("GM2", Qt::CaseInsensitive) == 0
        || name.compare("Ab2", Qt::CaseInsensitive) == 0
        || name.compare("Am2", Qt::CaseInsensitive) == 0
        || name.compare("G#2", Qt::CaseInsensitive) == 0
        || name.compare("G♯2", Qt::CaseInsensitive) == 0
        || name.compare("A♭2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(44);
    }

    else if (QString::number(45) == name
        || name.compare("La2", Qt::CaseInsensitive) == 0
        || name.compare("A2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(45);
    }

    else if (QString::number(46) == name
        || name.compare("Lad2", Qt::CaseInsensitive) == 0
        || name.compare("LaM2", Qt::CaseInsensitive) == 0
        || name.compare("Sib2", Qt::CaseInsensitive) == 0
        || name.compare("Sim2", Qt::CaseInsensitive) == 0
        || name.compare("La#2", Qt::CaseInsensitive) == 0
        || name.compare("La♯2", Qt::CaseInsensitive) == 0
        || name.compare("Si♭2", Qt::CaseInsensitive) == 0
        || name.compare("Ad2", Qt::CaseInsensitive) == 0
        || name.compare("AM2", Qt::CaseInsensitive) == 0
        || name.compare("Bb2", Qt::CaseInsensitive) == 0
        || name.compare("Bm2", Qt::CaseInsensitive) == 0
        || name.compare("A#2", Qt::CaseInsensitive) == 0
        || name.compare("A♯2", Qt::CaseInsensitive) == 0
        || name.compare("B♭2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(46);
    }

    else if (QString::number(47) == name
        || name.compare("Si2", Qt::CaseInsensitive) == 0
        || name.compare("Dob2", Qt::CaseInsensitive) == 0
        || name.compare("Dom2", Qt::CaseInsensitive) == 0
        || name.compare("B2", Qt::CaseInsensitive) == 0
        || name.compare("Cb2", Qt::CaseInsensitive) == 0
        || name.compare("Cm2", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(47);
    }

    else if (QString::number(48) == name
        || name.compare("Do3", Qt::CaseInsensitive) == 0
        || name.compare("Sid3", Qt::CaseInsensitive) == 0
        || name.compare("SiM3", Qt::CaseInsensitive) == 0
        || name.compare("C3", Qt::CaseInsensitive) == 0
        || name.compare("Bd3", Qt::CaseInsensitive) == 0
        || name.compare("BM3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(48);
    }

    else if (QString::number(49) == name
        || name.compare("Dod3", Qt::CaseInsensitive) == 0
        || name.compare("DoM3", Qt::CaseInsensitive) == 0
        || name.compare("Reb3", Qt::CaseInsensitive) == 0
        || name.compare("Rem3", Qt::CaseInsensitive) == 0
        || name.compare("Do#3", Qt::CaseInsensitive) == 0
        || name.compare("Do♯3", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭3", Qt::CaseInsensitive) == 0
        || name.compare("Re♭3", Qt::CaseInsensitive) == 0
        || name.compare("Réb3", Qt::CaseInsensitive) == 0
        || name.compare("Rém3", Qt::CaseInsensitive) == 0
        || name.compare("Cd3", Qt::CaseInsensitive) == 0
        || name.compare("CM3", Qt::CaseInsensitive) == 0
        || name.compare("Db3", Qt::CaseInsensitive) == 0
        || name.compare("Dm3", Qt::CaseInsensitive) == 0
        || name.compare("C#3", Qt::CaseInsensitive) == 0
        || name.compare("C♯3", Qt::CaseInsensitive) == 0
        || name.compare("D♭3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(49);
    }

    else if (QString::number(50) == name
        || name.compare("Re3", Qt::CaseInsensitive) == 0
        || name.compare("Ré3", Qt::CaseInsensitive) == 0
        || name.compare("D3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(50);
    }

    else if (QString::number(51) == name
        || name.compare("Red3", Qt::CaseInsensitive) == 0
        || name.compare("ReM3", Qt::CaseInsensitive) == 0
        || name.compare("Mib3", Qt::CaseInsensitive) == 0
        || name.compare("Mim3", Qt::CaseInsensitive) == 0
        || name.compare("Re#3", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯3", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭3", Qt::CaseInsensitive) == 0
        || name.compare("Re♯3", Qt::CaseInsensitive) == 0
        || name.compare("Ré#3", Qt::CaseInsensitive) == 0
        || name.compare("Réd3", Qt::CaseInsensitive) == 0
        || name.compare("RéM3", Qt::CaseInsensitive) == 0
        || name.compare("Dd3", Qt::CaseInsensitive) == 0
        || name.compare("DM3", Qt::CaseInsensitive) == 0
        || name.compare("Eb3", Qt::CaseInsensitive) == 0
        || name.compare("Em3", Qt::CaseInsensitive) == 0
        || name.compare("D#3", Qt::CaseInsensitive) == 0
        || name.compare("D♯3", Qt::CaseInsensitive) == 0
        || name.compare("E♭3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(51);
    }

    else if (QString::number(52) == name
        || name.compare("Mi3", Qt::CaseInsensitive) == 0
        || name.compare("Fab3", Qt::CaseInsensitive) == 0
        || name.compare("Fam3", Qt::CaseInsensitive) == 0
        || name.compare("E3", Qt::CaseInsensitive) == 0
        || name.compare("Fb3", Qt::CaseInsensitive) == 0
        || name.compare("Fm3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(52);
    }

    else if (QString::number(53) == name
        || name.compare("Fa3", Qt::CaseInsensitive) == 0
        || name.compare("Mid3", Qt::CaseInsensitive) == 0
        || name.compare("MiM3", Qt::CaseInsensitive) == 0
        || name.compare("F3", Qt::CaseInsensitive) == 0
        || name.compare("Ed3", Qt::CaseInsensitive) == 0
        || name.compare("EM3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(53);
    }

    else if (QString::number(54) == name
        || name.compare("Fad3", Qt::CaseInsensitive) == 0
        || name.compare("FaM3", Qt::CaseInsensitive) == 0
        || name.compare("Solb3", Qt::CaseInsensitive) == 0
        || name.compare("Solm3", Qt::CaseInsensitive) == 0
        || name.compare("Fa#3", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯3", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭3", Qt::CaseInsensitive) == 0
        || name.compare("Fd3", Qt::CaseInsensitive) == 0
        || name.compare("FM3", Qt::CaseInsensitive) == 0
        || name.compare("Gb3", Qt::CaseInsensitive) == 0
        || name.compare("Gm3", Qt::CaseInsensitive) == 0
        || name.compare("F#3", Qt::CaseInsensitive) == 0
        || name.compare("F♯3", Qt::CaseInsensitive) == 0
        || name.compare("G♭3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(54);
    }

    else if (QString::number(55) == name
        || name.compare("Sol3", Qt::CaseInsensitive) == 0
        || name.compare("G3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(55);
    }

    else if (QString::number(56) == name
        || name.compare("Sold3", Qt::CaseInsensitive) == 0
        || name.compare("SolM3", Qt::CaseInsensitive) == 0
        || name.compare("Lab3", Qt::CaseInsensitive) == 0
        || name.compare("Lam3", Qt::CaseInsensitive) == 0
        || name.compare("Sol#3", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯3", Qt::CaseInsensitive) == 0
        || name.compare("La♭3", Qt::CaseInsensitive) == 0
        || name.compare("Gd3", Qt::CaseInsensitive) == 0
        || name.compare("GM3", Qt::CaseInsensitive) == 0
        || name.compare("Ab3", Qt::CaseInsensitive) == 0
        || name.compare("Am3", Qt::CaseInsensitive) == 0
        || name.compare("G#3", Qt::CaseInsensitive) == 0
        || name.compare("G♯3", Qt::CaseInsensitive) == 0
        || name.compare("A♭3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(56);
    }

    else if (QString::number(57) == name
        || name.compare("La3", Qt::CaseInsensitive) == 0
        || name.compare("A3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(57);
    }

    else if (QString::number(58) == name
        || name.compare("Lad3", Qt::CaseInsensitive) == 0
        || name.compare("LaM3", Qt::CaseInsensitive) == 0
        || name.compare("Sib3", Qt::CaseInsensitive) == 0
        || name.compare("Sim3", Qt::CaseInsensitive) == 0
        || name.compare("La#3", Qt::CaseInsensitive) == 0
        || name.compare("La♯3", Qt::CaseInsensitive) == 0
        || name.compare("Si♭3", Qt::CaseInsensitive) == 0
        || name.compare("Ad3", Qt::CaseInsensitive) == 0
        || name.compare("AM3", Qt::CaseInsensitive) == 0
        || name.compare("Bb3", Qt::CaseInsensitive) == 0
        || name.compare("Bm3", Qt::CaseInsensitive) == 0
        || name.compare("A#3", Qt::CaseInsensitive) == 0
        || name.compare("A♯3", Qt::CaseInsensitive) == 0
        || name.compare("B♭3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(58);
    }

    else if (QString::number(59) == name
        || name.compare("Si3", Qt::CaseInsensitive) == 0
        || name.compare("Dob3", Qt::CaseInsensitive) == 0
        || name.compare("Dom3", Qt::CaseInsensitive) == 0
        || name.compare("B3", Qt::CaseInsensitive) == 0
        || name.compare("Cb3", Qt::CaseInsensitive) == 0
        || name.compare("Cm3", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(59);
    }

    else if (QString::number(60) == name
        || name.compare("Do4", Qt::CaseInsensitive) == 0
        || name.compare("Sid4", Qt::CaseInsensitive) == 0
        || name.compare("SiM4", Qt::CaseInsensitive) == 0
        || name.compare("C4", Qt::CaseInsensitive) == 0
        || name.compare("Bd4", Qt::CaseInsensitive) == 0
        || name.compare("BM4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(60);
    }

    else if (QString::number(61) == name
        || name.compare("Dod4", Qt::CaseInsensitive) == 0
        || name.compare("DoM4", Qt::CaseInsensitive) == 0
        || name.compare("Reb4", Qt::CaseInsensitive) == 0
        || name.compare("Rem4", Qt::CaseInsensitive) == 0
        || name.compare("Do#4", Qt::CaseInsensitive) == 0
        || name.compare("Do♯4", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭4", Qt::CaseInsensitive) == 0
        || name.compare("Re♭4", Qt::CaseInsensitive) == 0
        || name.compare("Réb4", Qt::CaseInsensitive) == 0
        || name.compare("Rém4", Qt::CaseInsensitive) == 0
        || name.compare("Cd4", Qt::CaseInsensitive) == 0
        || name.compare("CM4", Qt::CaseInsensitive) == 0
        || name.compare("Db4", Qt::CaseInsensitive) == 0
        || name.compare("Dm4", Qt::CaseInsensitive) == 0
        || name.compare("C#4", Qt::CaseInsensitive) == 0
        || name.compare("C♯4", Qt::CaseInsensitive) == 0
        || name.compare("D♭4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(61);
    }

    else if (QString::number(62) == name
        || name.compare("Re4", Qt::CaseInsensitive) == 0
        || name.compare("Ré4", Qt::CaseInsensitive) == 0
        || name.compare("D4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(62);
    }

    else if (QString::number(63) == name
        || name.compare("Red4", Qt::CaseInsensitive) == 0
        || name.compare("ReM4", Qt::CaseInsensitive) == 0
        || name.compare("Mib4", Qt::CaseInsensitive) == 0
        || name.compare("Mim4", Qt::CaseInsensitive) == 0
        || name.compare("Re#4", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯4", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭4", Qt::CaseInsensitive) == 0
        || name.compare("Re♯4", Qt::CaseInsensitive) == 0
        || name.compare("Ré#4", Qt::CaseInsensitive) == 0
        || name.compare("Réd4", Qt::CaseInsensitive) == 0
        || name.compare("RéM4", Qt::CaseInsensitive) == 0
        || name.compare("Dd4", Qt::CaseInsensitive) == 0
        || name.compare("DM4", Qt::CaseInsensitive) == 0
        || name.compare("Eb4", Qt::CaseInsensitive) == 0
        || name.compare("Em4", Qt::CaseInsensitive) == 0
        || name.compare("D#4", Qt::CaseInsensitive) == 0
        || name.compare("D♯4", Qt::CaseInsensitive) == 0
        || name.compare("E♭4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(63);
    }

    else if (QString::number(64) == name
        || name.compare("Mi4", Qt::CaseInsensitive) == 0
        || name.compare("Fab4", Qt::CaseInsensitive) == 0
        || name.compare("Fam4", Qt::CaseInsensitive) == 0
        || name.compare("E4", Qt::CaseInsensitive) == 0
        || name.compare("Fb4", Qt::CaseInsensitive) == 0
        || name.compare("Fm4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(64);
    }

    else if (QString::number(65) == name
        || name.compare("Fa4", Qt::CaseInsensitive) == 0
        || name.compare("Mid4", Qt::CaseInsensitive) == 0
        || name.compare("MiM4", Qt::CaseInsensitive) == 0
        || name.compare("F4", Qt::CaseInsensitive) == 0
        || name.compare("Ed4", Qt::CaseInsensitive) == 0
        || name.compare("EM4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(65);
    }

    else if (QString::number(66) == name
        || name.compare("Fad4", Qt::CaseInsensitive) == 0
        || name.compare("FaM4", Qt::CaseInsensitive) == 0
        || name.compare("Solb4", Qt::CaseInsensitive) == 0
        || name.compare("Solm4", Qt::CaseInsensitive) == 0
        || name.compare("Fa#4", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯4", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭4", Qt::CaseInsensitive) == 0
        || name.compare("Fd4", Qt::CaseInsensitive) == 0
        || name.compare("FM4", Qt::CaseInsensitive) == 0
        || name.compare("Gb4", Qt::CaseInsensitive) == 0
        || name.compare("Gm4", Qt::CaseInsensitive) == 0
        || name.compare("F#4", Qt::CaseInsensitive) == 0
        || name.compare("F♯4", Qt::CaseInsensitive) == 0
        || name.compare("G♭4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(66);
    }

    else if (QString::number(67) == name
        || name.compare("Sol4", Qt::CaseInsensitive) == 0
        || name.compare("G4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(67);
    }

    else if (QString::number(68) == name
        || name.compare("Sold4", Qt::CaseInsensitive) == 0
        || name.compare("SolM4", Qt::CaseInsensitive) == 0
        || name.compare("Lab4", Qt::CaseInsensitive) == 0
        || name.compare("Lam4", Qt::CaseInsensitive) == 0
        || name.compare("Sol#4", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯4", Qt::CaseInsensitive) == 0
        || name.compare("La♭4", Qt::CaseInsensitive) == 0
        || name.compare("Gd4", Qt::CaseInsensitive) == 0
        || name.compare("GM4", Qt::CaseInsensitive) == 0
        || name.compare("Ab4", Qt::CaseInsensitive) == 0
        || name.compare("Am4", Qt::CaseInsensitive) == 0
        || name.compare("G#4", Qt::CaseInsensitive) == 0
        || name.compare("G♯4", Qt::CaseInsensitive) == 0
        || name.compare("A♭4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(68);
    }

    else if (QString::number(69) == name
        || name.compare("La4", Qt::CaseInsensitive) == 0
        || name.compare("A4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(69);
    }

    else if (QString::number(70) == name
        || name.compare("Lad4", Qt::CaseInsensitive) == 0
        || name.compare("LaM4", Qt::CaseInsensitive) == 0
        || name.compare("Sib4", Qt::CaseInsensitive) == 0
        || name.compare("Sim4", Qt::CaseInsensitive) == 0
        || name.compare("La#4", Qt::CaseInsensitive) == 0
        || name.compare("La♯4", Qt::CaseInsensitive) == 0
        || name.compare("Si♭4", Qt::CaseInsensitive) == 0
        || name.compare("Ad4", Qt::CaseInsensitive) == 0
        || name.compare("AM4", Qt::CaseInsensitive) == 0
        || name.compare("Bb4", Qt::CaseInsensitive) == 0
        || name.compare("Bm4", Qt::CaseInsensitive) == 0
        || name.compare("A#4", Qt::CaseInsensitive) == 0
        || name.compare("A♯4", Qt::CaseInsensitive) == 0
        || name.compare("B♭4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(70);
    }

    else if (QString::number(71) == name
        || name.compare("Si4", Qt::CaseInsensitive) == 0
        || name.compare("Dob4", Qt::CaseInsensitive) == 0
        || name.compare("Dom4", Qt::CaseInsensitive) == 0
        || name.compare("B4", Qt::CaseInsensitive) == 0
        || name.compare("Cb4", Qt::CaseInsensitive) == 0
        || name.compare("Cm4", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(71);
    }

    else if (QString::number(72) == name
        || name.compare("Do5", Qt::CaseInsensitive) == 0
        || name.compare("Sid5", Qt::CaseInsensitive) == 0
        || name.compare("SiM5", Qt::CaseInsensitive) == 0
        || name.compare("C5", Qt::CaseInsensitive) == 0
        || name.compare("Bd5", Qt::CaseInsensitive) == 0
        || name.compare("BM5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(72);
    }

    else if (QString::number(73) == name
        || name.compare("Dod5", Qt::CaseInsensitive) == 0
        || name.compare("DoM5", Qt::CaseInsensitive) == 0
        || name.compare("Reb5", Qt::CaseInsensitive) == 0
        || name.compare("Rem5", Qt::CaseInsensitive) == 0
        || name.compare("Do#5", Qt::CaseInsensitive) == 0
        || name.compare("Do♯5", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭5", Qt::CaseInsensitive) == 0
        || name.compare("Re♭5", Qt::CaseInsensitive) == 0
        || name.compare("Réb5", Qt::CaseInsensitive) == 0
        || name.compare("Rém5", Qt::CaseInsensitive) == 0
        || name.compare("Cd5", Qt::CaseInsensitive) == 0
        || name.compare("CM5", Qt::CaseInsensitive) == 0
        || name.compare("Db5", Qt::CaseInsensitive) == 0
        || name.compare("Dm5", Qt::CaseInsensitive) == 0
        || name.compare("C#5", Qt::CaseInsensitive) == 0
        || name.compare("C♯5", Qt::CaseInsensitive) == 0
        || name.compare("D♭5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(73);
    }

    else if (QString::number(74) == name
        || name.compare("Re5", Qt::CaseInsensitive) == 0
        || name.compare("Ré5", Qt::CaseInsensitive) == 0
        || name.compare("D5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(74);
    }

    else if (QString::number(75) == name
        || name.compare("Red5", Qt::CaseInsensitive) == 0
        || name.compare("ReM5", Qt::CaseInsensitive) == 0
        || name.compare("Mib5", Qt::CaseInsensitive) == 0
        || name.compare("Mim5", Qt::CaseInsensitive) == 0
        || name.compare("Re#5", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯5", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭5", Qt::CaseInsensitive) == 0
        || name.compare("Re♯5", Qt::CaseInsensitive) == 0
        || name.compare("Ré#5", Qt::CaseInsensitive) == 0
        || name.compare("Réd5", Qt::CaseInsensitive) == 0
        || name.compare("RéM5", Qt::CaseInsensitive) == 0
        || name.compare("Dd5", Qt::CaseInsensitive) == 0
        || name.compare("DM5", Qt::CaseInsensitive) == 0
        || name.compare("Eb5", Qt::CaseInsensitive) == 0
        || name.compare("Em5", Qt::CaseInsensitive) == 0
        || name.compare("D#5", Qt::CaseInsensitive) == 0
        || name.compare("D♯5", Qt::CaseInsensitive) == 0
        || name.compare("E♭5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(75);
    }

    else if (QString::number(76) == name
        || name.compare("Mi5", Qt::CaseInsensitive) == 0
        || name.compare("Fab5", Qt::CaseInsensitive) == 0
        || name.compare("Fam5", Qt::CaseInsensitive) == 0
        || name.compare("E5", Qt::CaseInsensitive) == 0
        || name.compare("Fb5", Qt::CaseInsensitive) == 0
        || name.compare("Fm5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(76);
    }

    else if (QString::number(77) == name
        || name.compare("Fa5", Qt::CaseInsensitive) == 0
        || name.compare("Mid5", Qt::CaseInsensitive) == 0
        || name.compare("MiM5", Qt::CaseInsensitive) == 0
        || name.compare("F5", Qt::CaseInsensitive) == 0
        || name.compare("Ed5", Qt::CaseInsensitive) == 0
        || name.compare("EM5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(77);
    }

    else if (QString::number(78) == name
        || name.compare("Fad5", Qt::CaseInsensitive) == 0
        || name.compare("FaM5", Qt::CaseInsensitive) == 0
        || name.compare("Solb5", Qt::CaseInsensitive) == 0
        || name.compare("Solm5", Qt::CaseInsensitive) == 0
        || name.compare("Fa#5", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯5", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭5", Qt::CaseInsensitive) == 0
        || name.compare("Fd5", Qt::CaseInsensitive) == 0
        || name.compare("FM5", Qt::CaseInsensitive) == 0
        || name.compare("Gb5", Qt::CaseInsensitive) == 0
        || name.compare("Gm5", Qt::CaseInsensitive) == 0
        || name.compare("F#5", Qt::CaseInsensitive) == 0
        || name.compare("F♯5", Qt::CaseInsensitive) == 0
        || name.compare("G♭5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(78);
    }

    else if (QString::number(79) == name
        || name.compare("Sol5", Qt::CaseInsensitive) == 0
        || name.compare("G5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(79);
    }

    else if (QString::number(80) == name
        || name.compare("Sold5", Qt::CaseInsensitive) == 0
        || name.compare("SolM5", Qt::CaseInsensitive) == 0
        || name.compare("Lab5", Qt::CaseInsensitive) == 0
        || name.compare("Lam5", Qt::CaseInsensitive) == 0
        || name.compare("Sol#5", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯5", Qt::CaseInsensitive) == 0
        || name.compare("La♭5", Qt::CaseInsensitive) == 0
        || name.compare("Gd5", Qt::CaseInsensitive) == 0
        || name.compare("GM5", Qt::CaseInsensitive) == 0
        || name.compare("Ab5", Qt::CaseInsensitive) == 0
        || name.compare("Am5", Qt::CaseInsensitive) == 0
        || name.compare("G#5", Qt::CaseInsensitive) == 0
        || name.compare("G♯5", Qt::CaseInsensitive) == 0
        || name.compare("A♭5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(80);
    }

    else if (QString::number(81) == name
        || name.compare("La5", Qt::CaseInsensitive) == 0
        || name.compare("A5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(81);
    }

    else if (QString::number(82) == name
        || name.compare("Lad5", Qt::CaseInsensitive) == 0
        || name.compare("LaM5", Qt::CaseInsensitive) == 0
        || name.compare("Sib5", Qt::CaseInsensitive) == 0
        || name.compare("Sim5", Qt::CaseInsensitive) == 0
        || name.compare("La#5", Qt::CaseInsensitive) == 0
        || name.compare("La♯5", Qt::CaseInsensitive) == 0
        || name.compare("Si♭5", Qt::CaseInsensitive) == 0
        || name.compare("Ad5", Qt::CaseInsensitive) == 0
        || name.compare("AM5", Qt::CaseInsensitive) == 0
        || name.compare("Bb5", Qt::CaseInsensitive) == 0
        || name.compare("Bm5", Qt::CaseInsensitive) == 0
        || name.compare("A#5", Qt::CaseInsensitive) == 0
        || name.compare("A♯5", Qt::CaseInsensitive) == 0
        || name.compare("B♭5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(82);
    }

    else if (QString::number(83) == name
        || name.compare("Si5", Qt::CaseInsensitive) == 0
        || name.compare("Dob5", Qt::CaseInsensitive) == 0
        || name.compare("Dom5", Qt::CaseInsensitive) == 0
        || name.compare("B5", Qt::CaseInsensitive) == 0
        || name.compare("Cb5", Qt::CaseInsensitive) == 0
        || name.compare("Cm5", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(83);
    }

    else if (QString::number(84) == name
        || name.compare("Do6", Qt::CaseInsensitive) == 0
        || name.compare("Sid6", Qt::CaseInsensitive) == 0
        || name.compare("SiM6", Qt::CaseInsensitive) == 0
        || name.compare("C6", Qt::CaseInsensitive) == 0
        || name.compare("Bd6", Qt::CaseInsensitive) == 0
        || name.compare("BM6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(84);
    }

    else if (QString::number(85) == name
        || name.compare("Dod6", Qt::CaseInsensitive) == 0
        || name.compare("DoM6", Qt::CaseInsensitive) == 0
        || name.compare("Reb6", Qt::CaseInsensitive) == 0
        || name.compare("Rem6", Qt::CaseInsensitive) == 0
        || name.compare("Do#6", Qt::CaseInsensitive) == 0
        || name.compare("Do♯6", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭6", Qt::CaseInsensitive) == 0
        || name.compare("Re♭6", Qt::CaseInsensitive) == 0
        || name.compare("Réb6", Qt::CaseInsensitive) == 0
        || name.compare("Rém6", Qt::CaseInsensitive) == 0
        || name.compare("Cd6", Qt::CaseInsensitive) == 0
        || name.compare("CM6", Qt::CaseInsensitive) == 0
        || name.compare("Db6", Qt::CaseInsensitive) == 0
        || name.compare("Dm6", Qt::CaseInsensitive) == 0
        || name.compare("C#6", Qt::CaseInsensitive) == 0
        || name.compare("C♯6", Qt::CaseInsensitive) == 0
        || name.compare("D♭6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(85);
    }

    else if (QString::number(86) == name
        || name.compare("Re6", Qt::CaseInsensitive) == 0
        || name.compare("Ré6", Qt::CaseInsensitive) == 0
        || name.compare("D6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(86);
    }

    else if (QString::number(87) == name
        || name.compare("Red6", Qt::CaseInsensitive) == 0
        || name.compare("ReM6", Qt::CaseInsensitive) == 0
        || name.compare("Mib6", Qt::CaseInsensitive) == 0
        || name.compare("Mim6", Qt::CaseInsensitive) == 0
        || name.compare("Re#6", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯6", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭6", Qt::CaseInsensitive) == 0
        || name.compare("Re♯6", Qt::CaseInsensitive) == 0
        || name.compare("Ré#6", Qt::CaseInsensitive) == 0
        || name.compare("Réd6", Qt::CaseInsensitive) == 0
        || name.compare("RéM6", Qt::CaseInsensitive) == 0
        || name.compare("Dd6", Qt::CaseInsensitive) == 0
        || name.compare("DM6", Qt::CaseInsensitive) == 0
        || name.compare("Eb6", Qt::CaseInsensitive) == 0
        || name.compare("Em6", Qt::CaseInsensitive) == 0
        || name.compare("D#6", Qt::CaseInsensitive) == 0
        || name.compare("D♯6", Qt::CaseInsensitive) == 0
        || name.compare("E♭6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(87);
    }

    else if (QString::number(88) == name
        || name.compare("Mi6", Qt::CaseInsensitive) == 0
        || name.compare("Fab6", Qt::CaseInsensitive) == 0
        || name.compare("Fam6", Qt::CaseInsensitive) == 0
        || name.compare("E6", Qt::CaseInsensitive) == 0
        || name.compare("Fb6", Qt::CaseInsensitive) == 0
        || name.compare("Fm6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(88);
    }

    else if (QString::number(89) == name
        || name.compare("Fa6", Qt::CaseInsensitive) == 0
        || name.compare("Mid6", Qt::CaseInsensitive) == 0
        || name.compare("MiM6", Qt::CaseInsensitive) == 0
        || name.compare("F6", Qt::CaseInsensitive) == 0
        || name.compare("Ed6", Qt::CaseInsensitive) == 0
        || name.compare("EM6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(89);
    }

    else if (QString::number(90) == name
        || name.compare("Fad6", Qt::CaseInsensitive) == 0
        || name.compare("FaM6", Qt::CaseInsensitive) == 0
        || name.compare("Solb6", Qt::CaseInsensitive) == 0
        || name.compare("Solm6", Qt::CaseInsensitive) == 0
        || name.compare("Fa#6", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯6", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭6", Qt::CaseInsensitive) == 0
        || name.compare("Fd6", Qt::CaseInsensitive) == 0
        || name.compare("FM6", Qt::CaseInsensitive) == 0
        || name.compare("Gb6", Qt::CaseInsensitive) == 0
        || name.compare("Gm6", Qt::CaseInsensitive) == 0
        || name.compare("F#6", Qt::CaseInsensitive) == 0
        || name.compare("F♯6", Qt::CaseInsensitive) == 0
        || name.compare("G♭6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(90);
    }

    else if (QString::number(91) == name
        || name.compare("Sol6", Qt::CaseInsensitive) == 0
        || name.compare("G6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(91);
    }

    else if (QString::number(92) == name
        || name.compare("Sold6", Qt::CaseInsensitive) == 0
        || name.compare("SolM6", Qt::CaseInsensitive) == 0
        || name.compare("Lab6", Qt::CaseInsensitive) == 0
        || name.compare("Lam6", Qt::CaseInsensitive) == 0
        || name.compare("Sol#6", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯6", Qt::CaseInsensitive) == 0
        || name.compare("La♭6", Qt::CaseInsensitive) == 0
        || name.compare("Gd6", Qt::CaseInsensitive) == 0
        || name.compare("GM6", Qt::CaseInsensitive) == 0
        || name.compare("Ab6", Qt::CaseInsensitive) == 0
        || name.compare("Am6", Qt::CaseInsensitive) == 0
        || name.compare("G#6", Qt::CaseInsensitive) == 0
        || name.compare("G♯6", Qt::CaseInsensitive) == 0
        || name.compare("A♭6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(92);
    }

    else if (QString::number(93) == name
        || name.compare("La6", Qt::CaseInsensitive) == 0
        || name.compare("A6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(93);
    }

    else if (QString::number(94) == name
        || name.compare("Lad6", Qt::CaseInsensitive) == 0
        || name.compare("LaM6", Qt::CaseInsensitive) == 0
        || name.compare("Sib6", Qt::CaseInsensitive) == 0
        || name.compare("Sim6", Qt::CaseInsensitive) == 0
        || name.compare("La#6", Qt::CaseInsensitive) == 0
        || name.compare("La♯6", Qt::CaseInsensitive) == 0
        || name.compare("Si♭6", Qt::CaseInsensitive) == 0
        || name.compare("Ad6", Qt::CaseInsensitive) == 0
        || name.compare("AM6", Qt::CaseInsensitive) == 0
        || name.compare("Bb6", Qt::CaseInsensitive) == 0
        || name.compare("Bm6", Qt::CaseInsensitive) == 0
        || name.compare("A#6", Qt::CaseInsensitive) == 0
        || name.compare("A♯6", Qt::CaseInsensitive) == 0
        || name.compare("B♭6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(94);
    }

    else if (QString::number(95) == name
        || name.compare("Si6", Qt::CaseInsensitive) == 0
        || name.compare("Dob6", Qt::CaseInsensitive) == 0
        || name.compare("Dom6", Qt::CaseInsensitive) == 0
        || name.compare("B6", Qt::CaseInsensitive) == 0
        || name.compare("Cb6", Qt::CaseInsensitive) == 0
        || name.compare("Cm6", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(95);
    }

    else if (QString::number(96) == name
        || name.compare("Do7", Qt::CaseInsensitive) == 0
        || name.compare("Sid7", Qt::CaseInsensitive) == 0
        || name.compare("SiM7", Qt::CaseInsensitive) == 0
        || name.compare("C7", Qt::CaseInsensitive) == 0
        || name.compare("Bd7", Qt::CaseInsensitive) == 0
        || name.compare("BM7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(96);
    }

    else if (QString::number(97) == name
        || name.compare("Dod7", Qt::CaseInsensitive) == 0
        || name.compare("DoM7", Qt::CaseInsensitive) == 0
        || name.compare("Reb7", Qt::CaseInsensitive) == 0
        || name.compare("Rem7", Qt::CaseInsensitive) == 0
        || name.compare("Do#7", Qt::CaseInsensitive) == 0
        || name.compare("Do♯7", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭7", Qt::CaseInsensitive) == 0
        || name.compare("Re♭7", Qt::CaseInsensitive) == 0
        || name.compare("Réb7", Qt::CaseInsensitive) == 0
        || name.compare("Rém7", Qt::CaseInsensitive) == 0
        || name.compare("Cd7", Qt::CaseInsensitive) == 0
        || name.compare("CM7", Qt::CaseInsensitive) == 0
        || name.compare("Db7", Qt::CaseInsensitive) == 0
        || name.compare("Dm7", Qt::CaseInsensitive) == 0
        || name.compare("C#7", Qt::CaseInsensitive) == 0
        || name.compare("C♯7", Qt::CaseInsensitive) == 0
        || name.compare("D♭7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(97);
    }

    else if (QString::number(98) == name
        || name.compare("Re7", Qt::CaseInsensitive) == 0
        || name.compare("Ré7", Qt::CaseInsensitive) == 0
        || name.compare("D7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(98);
    }

    else if (QString::number(99) == name
        || name.compare("Red7", Qt::CaseInsensitive) == 0
        || name.compare("ReM7", Qt::CaseInsensitive) == 0
        || name.compare("Mib7", Qt::CaseInsensitive) == 0
        || name.compare("Mim7", Qt::CaseInsensitive) == 0
        || name.compare("Re#7", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯7", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭7", Qt::CaseInsensitive) == 0
        || name.compare("Re♯7", Qt::CaseInsensitive) == 0
        || name.compare("Ré#7", Qt::CaseInsensitive) == 0
        || name.compare("Réd7", Qt::CaseInsensitive) == 0
        || name.compare("RéM7", Qt::CaseInsensitive) == 0
        || name.compare("Dd7", Qt::CaseInsensitive) == 0
        || name.compare("DM7", Qt::CaseInsensitive) == 0
        || name.compare("Eb7", Qt::CaseInsensitive) == 0
        || name.compare("Em7", Qt::CaseInsensitive) == 0
        || name.compare("D#7", Qt::CaseInsensitive) == 0
        || name.compare("D♯7", Qt::CaseInsensitive) == 0
        || name.compare("E♭7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(99);
    }

    else if (QString::number(100) == name
        || name.compare("Mi7", Qt::CaseInsensitive) == 0
        || name.compare("Fab7", Qt::CaseInsensitive) == 0
        || name.compare("Fam7", Qt::CaseInsensitive) == 0
        || name.compare("E7", Qt::CaseInsensitive) == 0
        || name.compare("Fb7", Qt::CaseInsensitive) == 0
        || name.compare("Fm7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(100);
    }

    else if (QString::number(101) == name
        || name.compare("Fa7", Qt::CaseInsensitive) == 0
        || name.compare("Mid7", Qt::CaseInsensitive) == 0
        || name.compare("MiM7", Qt::CaseInsensitive) == 0
        || name.compare("F7", Qt::CaseInsensitive) == 0
        || name.compare("Ed7", Qt::CaseInsensitive) == 0
        || name.compare("EM7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(101);
    }

    else if (QString::number(102) == name
        || name.compare("Fad7", Qt::CaseInsensitive) == 0
        || name.compare("FaM7", Qt::CaseInsensitive) == 0
        || name.compare("Solb7", Qt::CaseInsensitive) == 0
        || name.compare("Solm7", Qt::CaseInsensitive) == 0
        || name.compare("Fa#7", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯7", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭7", Qt::CaseInsensitive) == 0
        || name.compare("Fd7", Qt::CaseInsensitive) == 0
        || name.compare("FM7", Qt::CaseInsensitive) == 0
        || name.compare("Gb7", Qt::CaseInsensitive) == 0
        || name.compare("Gm7", Qt::CaseInsensitive) == 0
        || name.compare("F#7", Qt::CaseInsensitive) == 0
        || name.compare("F♯7", Qt::CaseInsensitive) == 0
        || name.compare("G♭7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(102);
    }

    else if (QString::number(103) == name
        || name.compare("Sol7", Qt::CaseInsensitive) == 0
        || name.compare("G7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(103);
    }

    else if (QString::number(104) == name
        || name.compare("Sold7", Qt::CaseInsensitive) == 0
        || name.compare("SolM7", Qt::CaseInsensitive) == 0
        || name.compare("Lab7", Qt::CaseInsensitive) == 0
        || name.compare("Lam7", Qt::CaseInsensitive) == 0
        || name.compare("Sol#7", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯7", Qt::CaseInsensitive) == 0
        || name.compare("La♭7", Qt::CaseInsensitive) == 0
        || name.compare("Gd7", Qt::CaseInsensitive) == 0
        || name.compare("GM7", Qt::CaseInsensitive) == 0
        || name.compare("Ab7", Qt::CaseInsensitive) == 0
        || name.compare("Am7", Qt::CaseInsensitive) == 0
        || name.compare("G#7", Qt::CaseInsensitive) == 0
        || name.compare("G♯7", Qt::CaseInsensitive) == 0
        || name.compare("A♭7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(104);
    }

    else if (QString::number(105) == name
        || name.compare("La7", Qt::CaseInsensitive) == 0
        || name.compare("A7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(105);
    }

    else if (QString::number(106) == name
        || name.compare("Lad7", Qt::CaseInsensitive) == 0
        || name.compare("LaM7", Qt::CaseInsensitive) == 0
        || name.compare("Sib7", Qt::CaseInsensitive) == 0
        || name.compare("Sim7", Qt::CaseInsensitive) == 0
        || name.compare("La#7", Qt::CaseInsensitive) == 0
        || name.compare("La♯7", Qt::CaseInsensitive) == 0
        || name.compare("Si♭7", Qt::CaseInsensitive) == 0
        || name.compare("Ad7", Qt::CaseInsensitive) == 0
        || name.compare("AM7", Qt::CaseInsensitive) == 0
        || name.compare("Bb7", Qt::CaseInsensitive) == 0
        || name.compare("Bm7", Qt::CaseInsensitive) == 0
        || name.compare("A#7", Qt::CaseInsensitive) == 0
        || name.compare("A♯7", Qt::CaseInsensitive) == 0
        || name.compare("B♭7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(106);
    }

    else if (QString::number(107) == name
        || name.compare("Si7", Qt::CaseInsensitive) == 0
        || name.compare("Dob7", Qt::CaseInsensitive) == 0
        || name.compare("Dom7", Qt::CaseInsensitive) == 0
        || name.compare("B7", Qt::CaseInsensitive) == 0
        || name.compare("Cb7", Qt::CaseInsensitive) == 0
        || name.compare("Cm7", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(107);
    }

    else if (QString::number(108) == name
        || name.compare("Do8", Qt::CaseInsensitive) == 0
        || name.compare("Sid8", Qt::CaseInsensitive) == 0
        || name.compare("SiM8", Qt::CaseInsensitive) == 0
        || name.compare("C8", Qt::CaseInsensitive) == 0
        || name.compare("Bd8", Qt::CaseInsensitive) == 0
        || name.compare("BM8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(108);
    }

    else if (QString::number(109) == name
        || name.compare("Dod8", Qt::CaseInsensitive) == 0
        || name.compare("DoM8", Qt::CaseInsensitive) == 0
        || name.compare("Reb8", Qt::CaseInsensitive) == 0
        || name.compare("Rem8", Qt::CaseInsensitive) == 0
        || name.compare("Do#8", Qt::CaseInsensitive) == 0
        || name.compare("Do♯8", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭8", Qt::CaseInsensitive) == 0
        || name.compare("Re♭8", Qt::CaseInsensitive) == 0
        || name.compare("Réb8", Qt::CaseInsensitive) == 0
        || name.compare("Rém8", Qt::CaseInsensitive) == 0
        || name.compare("Cd8", Qt::CaseInsensitive) == 0
        || name.compare("CM8", Qt::CaseInsensitive) == 0
        || name.compare("Db8", Qt::CaseInsensitive) == 0
        || name.compare("Dm8", Qt::CaseInsensitive) == 0
        || name.compare("C#8", Qt::CaseInsensitive) == 0
        || name.compare("C♯8", Qt::CaseInsensitive) == 0
        || name.compare("D♭8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(109);
    }

    else if (QString::number(110) == name
        || name.compare("Re8", Qt::CaseInsensitive) == 0
        || name.compare("Ré8", Qt::CaseInsensitive) == 0
        || name.compare("D8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(110);
    }

    else if (QString::number(111) == name
        || name.compare("Red8", Qt::CaseInsensitive) == 0
        || name.compare("ReM8", Qt::CaseInsensitive) == 0
        || name.compare("Mib8", Qt::CaseInsensitive) == 0
        || name.compare("Mim8", Qt::CaseInsensitive) == 0
        || name.compare("Re#8", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯8", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭8", Qt::CaseInsensitive) == 0
        || name.compare("Re♯8", Qt::CaseInsensitive) == 0
        || name.compare("Ré#8", Qt::CaseInsensitive) == 0
        || name.compare("Réd8", Qt::CaseInsensitive) == 0
        || name.compare("RéM8", Qt::CaseInsensitive) == 0
        || name.compare("Dd8", Qt::CaseInsensitive) == 0
        || name.compare("DM8", Qt::CaseInsensitive) == 0
        || name.compare("Eb8", Qt::CaseInsensitive) == 0
        || name.compare("Em8", Qt::CaseInsensitive) == 0
        || name.compare("D#8", Qt::CaseInsensitive) == 0
        || name.compare("D♯8", Qt::CaseInsensitive) == 0
        || name.compare("E♭8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(111);
    }

    else if (QString::number(112) == name
        || name.compare("Mi8", Qt::CaseInsensitive) == 0
        || name.compare("Fab8", Qt::CaseInsensitive) == 0
        || name.compare("Fam8", Qt::CaseInsensitive) == 0
        || name.compare("E8", Qt::CaseInsensitive) == 0
        || name.compare("Fb8", Qt::CaseInsensitive) == 0
        || name.compare("Fm8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(112);
    }

    else if (QString::number(113) == name
        || name.compare("Fa8", Qt::CaseInsensitive) == 0
        || name.compare("Mid8", Qt::CaseInsensitive) == 0
        || name.compare("MiM8", Qt::CaseInsensitive) == 0
        || name.compare("F8", Qt::CaseInsensitive) == 0
        || name.compare("Ed8", Qt::CaseInsensitive) == 0
        || name.compare("EM8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(113);
    }

    else if (QString::number(114) == name
        || name.compare("Fad8", Qt::CaseInsensitive) == 0
        || name.compare("FaM8", Qt::CaseInsensitive) == 0
        || name.compare("Solb8", Qt::CaseInsensitive) == 0
        || name.compare("Solm8", Qt::CaseInsensitive) == 0
        || name.compare("Fa#8", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯8", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭8", Qt::CaseInsensitive) == 0
        || name.compare("Fd8", Qt::CaseInsensitive) == 0
        || name.compare("FM8", Qt::CaseInsensitive) == 0
        || name.compare("Gb8", Qt::CaseInsensitive) == 0
        || name.compare("Gm8", Qt::CaseInsensitive) == 0
        || name.compare("F#8", Qt::CaseInsensitive) == 0
        || name.compare("F♯8", Qt::CaseInsensitive) == 0
        || name.compare("G♭8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(114);
    }

    else if (QString::number(115) == name
        || name.compare("Sol8", Qt::CaseInsensitive) == 0
        || name.compare("G8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(115);
    }

    else if (QString::number(116) == name
        || name.compare("Sold8", Qt::CaseInsensitive) == 0
        || name.compare("SolM8", Qt::CaseInsensitive) == 0
        || name.compare("Lab8", Qt::CaseInsensitive) == 0
        || name.compare("Lam8", Qt::CaseInsensitive) == 0
        || name.compare("Sol#8", Qt::CaseInsensitive) == 0
        || name.compare("Sol♯8", Qt::CaseInsensitive) == 0
        || name.compare("La♭8", Qt::CaseInsensitive) == 0
        || name.compare("Gd8", Qt::CaseInsensitive) == 0
        || name.compare("GM8", Qt::CaseInsensitive) == 0
        || name.compare("Ab8", Qt::CaseInsensitive) == 0
        || name.compare("Am8", Qt::CaseInsensitive) == 0
        || name.compare("G#8", Qt::CaseInsensitive) == 0
        || name.compare("G♯8", Qt::CaseInsensitive) == 0
        || name.compare("A♭8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(116);
    }

    else if (QString::number(117) == name
        || name.compare("La8", Qt::CaseInsensitive) == 0
        || name.compare("A8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(117);
    }

    else if (QString::number(118) == name
        || name.compare("Lad8", Qt::CaseInsensitive) == 0
        || name.compare("LaM8", Qt::CaseInsensitive) == 0
        || name.compare("Sib8", Qt::CaseInsensitive) == 0
        || name.compare("Sim8", Qt::CaseInsensitive) == 0
        || name.compare("La#8", Qt::CaseInsensitive) == 0
        || name.compare("La♯8", Qt::CaseInsensitive) == 0
        || name.compare("Si♭8", Qt::CaseInsensitive) == 0
        || name.compare("Ad8", Qt::CaseInsensitive) == 0
        || name.compare("AM8", Qt::CaseInsensitive) == 0
        || name.compare("Bb8", Qt::CaseInsensitive) == 0
        || name.compare("Bm8", Qt::CaseInsensitive) == 0
        || name.compare("A#8", Qt::CaseInsensitive) == 0
        || name.compare("A♯8", Qt::CaseInsensitive) == 0
        || name.compare("B♭8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(118);
    }

    else if (QString::number(119) == name
        || name.compare("Si8", Qt::CaseInsensitive) == 0
        || name.compare("Dob8", Qt::CaseInsensitive) == 0
        || name.compare("Dom8", Qt::CaseInsensitive) == 0
        || name.compare("B8", Qt::CaseInsensitive) == 0
        || name.compare("Cb8", Qt::CaseInsensitive) == 0
        || name.compare("Cm8", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(119);
    }

    else if (QString::number(120) == name
        || name.compare("Do9", Qt::CaseInsensitive) == 0
        || name.compare("Sid9", Qt::CaseInsensitive) == 0
        || name.compare("SiM9", Qt::CaseInsensitive) == 0
        || name.compare("C9", Qt::CaseInsensitive) == 0
        || name.compare("Bd9", Qt::CaseInsensitive) == 0
        || name.compare("BM9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(120);
    }

    else if (QString::number(121) == name
        || name.compare("Dod9", Qt::CaseInsensitive) == 0
        || name.compare("DoM9", Qt::CaseInsensitive) == 0
        || name.compare("Reb9", Qt::CaseInsensitive) == 0
        || name.compare("Rem9", Qt::CaseInsensitive) == 0
        || name.compare("Do#9", Qt::CaseInsensitive) == 0
        || name.compare("Do♯9", Qt::CaseInsensitive) == 0
        || name.compare("Ré♭9", Qt::CaseInsensitive) == 0
        || name.compare("Re♭9", Qt::CaseInsensitive) == 0
        || name.compare("Réb9", Qt::CaseInsensitive) == 0
        || name.compare("Rém9", Qt::CaseInsensitive) == 0
        || name.compare("Cd9", Qt::CaseInsensitive) == 0
        || name.compare("CM9", Qt::CaseInsensitive) == 0
        || name.compare("Db9", Qt::CaseInsensitive) == 0
        || name.compare("Dm9", Qt::CaseInsensitive) == 0
        || name.compare("C#9", Qt::CaseInsensitive) == 0
        || name.compare("C♯9", Qt::CaseInsensitive) == 0
        || name.compare("D♭9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(121);
    }

    else if (QString::number(122) == name
        || name.compare("Re9", Qt::CaseInsensitive) == 0
        || name.compare("Ré9", Qt::CaseInsensitive) == 0
        || name.compare("D9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(122);
    }

    else if (QString::number(123) == name
        || name.compare("Red9", Qt::CaseInsensitive) == 0
        || name.compare("ReM9", Qt::CaseInsensitive) == 0
        || name.compare("Mib9", Qt::CaseInsensitive) == 0
        || name.compare("Mim9", Qt::CaseInsensitive) == 0
        || name.compare("Re#9", Qt::CaseInsensitive) == 0
        || name.compare("Ré♯9", Qt::CaseInsensitive) == 0
        || name.compare("Mi♭9", Qt::CaseInsensitive) == 0
        || name.compare("Re♯9", Qt::CaseInsensitive) == 0
        || name.compare("Ré#9", Qt::CaseInsensitive) == 0
        || name.compare("Réd9", Qt::CaseInsensitive) == 0
        || name.compare("RéM9", Qt::CaseInsensitive) == 0
        || name.compare("Dd9", Qt::CaseInsensitive) == 0
        || name.compare("DM9", Qt::CaseInsensitive) == 0
        || name.compare("Eb9", Qt::CaseInsensitive) == 0
        || name.compare("Em9", Qt::CaseInsensitive) == 0
        || name.compare("D#9", Qt::CaseInsensitive) == 0
        || name.compare("D♯9", Qt::CaseInsensitive) == 0
        || name.compare("E♭9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(123);
    }

    else if (QString::number(124) == name
        || name.compare("Mi9", Qt::CaseInsensitive) == 0
        || name.compare("Fab9", Qt::CaseInsensitive) == 0
        || name.compare("Fam9", Qt::CaseInsensitive) == 0
        || name.compare("E9", Qt::CaseInsensitive) == 0
        || name.compare("Fb9", Qt::CaseInsensitive) == 0
        || name.compare("Fm9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(124);
    }

    else if (QString::number(125) == name
        || name.compare("Fa9", Qt::CaseInsensitive) == 0
        || name.compare("Mid9", Qt::CaseInsensitive) == 0
        || name.compare("MiM9", Qt::CaseInsensitive) == 0
        || name.compare("F9", Qt::CaseInsensitive) == 0
        || name.compare("Ed9", Qt::CaseInsensitive) == 0
        || name.compare("EM9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(125);
    }

    else if (QString::number(126) == name
        || name.compare("Fad9", Qt::CaseInsensitive) == 0
        || name.compare("FaM9", Qt::CaseInsensitive) == 0
        || name.compare("Solb9", Qt::CaseInsensitive) == 0
        || name.compare("Solm9", Qt::CaseInsensitive) == 0
        || name.compare("Fa#9", Qt::CaseInsensitive) == 0
        || name.compare("Fa♯9", Qt::CaseInsensitive) == 0
        || name.compare("Sol♭9", Qt::CaseInsensitive) == 0
        || name.compare("Fd9", Qt::CaseInsensitive) == 0
        || name.compare("FM9", Qt::CaseInsensitive) == 0
        || name.compare("Gb9", Qt::CaseInsensitive) == 0
        || name.compare("Gm9", Qt::CaseInsensitive) == 0
        || name.compare("F#9", Qt::CaseInsensitive) == 0
        || name.compare("F♯9", Qt::CaseInsensitive) == 0
        || name.compare("G♭9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(126);
    }

    else if (QString::number(127) == name
        || name.compare("Sol9", Qt::CaseInsensitive) == 0
        || name.compare("G9", Qt::CaseInsensitive) == 0
    ) {
            return QMidiNote(127);
    }

    else throw QMidiNoteException(QMidiNoteException::UnknownNote, -1);
}
